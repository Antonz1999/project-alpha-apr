from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.forms import CreateProjectForm


@login_required
def list_projects(request):
    project_list = Project.objects.filter(owner=request.user)
    context = {
        "project_list": project_list,
    }
    return render(request, "projects/list.html", context)


@login_required
def project_detail(request, id):
    detail = get_object_or_404(Project, id=id)
    context = {
        "project_detail": detail,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    form = CreateProjectForm()
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            project_item = form.save(commit=False)
            project_item.purchaser = request.user
            project_item.save()
            return redirect("list_projects")

    context = {
        "form": form,
    }

    return render(request, "projects/create.html", context)
