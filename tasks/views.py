from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTaskForm
from tasks.models import Task


@login_required
def create_task(request):
    form = CreateTaskForm()
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            task_item = form.save(commit=False)
            task_item.purchaser = request.user
            task_item.save()
            return redirect("list_projects")

    context = {
        "form": form,
    }

    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    task_list = Task.objects.filter(assignee=request.user)
    context = {
        "task_list": task_list,
    }
    return render(request, "tasks/detail.html", context)
